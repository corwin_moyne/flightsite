'use strict';

angular.module('angular10App')
    .config(function ($stateProvider) {
        $stateProvider
            .state('results', {
                url: '/results',
                params: {
                    flightType: null,
                    from: null,
                    to: null,
                    depDate: null,
                    arrDate: null,
                    class: null,
                    adults: null,
                    children: null,
                    infants: null
                },
                templateUrl: 'app/results/results.html',
                controller: 'ResultsCtrl',
                resolve: {
                    results: function (resultsService, $stateParams) {
                        
                        return resultsService.getResults($stateParams);
                    }
                }
            });
    });