'use strict';

angular.module('angular10App')
    .controller('ResultsCtrl', function ($scope, $stateParams, results) {

        $scope.flightType = $stateParams.flightType;
        $scope.selectedAirportDep = $stateParams.from;
        $scope.selectedAirportRet = $stateParams.to;
        $scope.depDate = $stateParams.depDate;
        $scope.arrDate = $stateParams.arrDate;
        $scope.class = $stateParams.class;
        $scope.adults = $stateParams.adults;
        $scope.children = $stateParams.children;

        $scope.results = results;

        $scope.selectedOutboundFlight = results[0].id;
        $scope.selectedInboundFlight = results[0].id;

        console.log($scope.results);
    
        $scope.getPrice = function(){
            
        };

    });