

'use strict';

class SearchViewConfigText {

	//Contructor
    constructor() {

        //Text variables
        this.outboundAirportText = null;
        this.inboundAirportText = null;
        this.departDateText = null;
        this.returnDateText = null;
        this.cabinText = null;
        this.adultComboText = null;
        this.childrenComboText = null;
        this.infantComboText = null;
    }
}
