'use strict';
angular.module('angular10App')
    .controller('SelectFlightsCtrl', function ($scope, $localStorage, $sessionStorage, $filter, airports, Modal, SearchFlightObject) {

        //        $scope.$storage = $sessionStorage;

        //        $scope.$storage = $localStorage.$default({
        //            selectedAirportDep: null
        //        });

        // var searchFlightObject = new SearchFlightObject();

        let searchViewConfig = new SearchViewConfig();
        searchViewConfig.maxPassengers();

        // console.log('search: ' + searchViewConfig.maxPassengers());

        //airportService
        $scope.airports = airports;

        //Variables
        var maxPassengers = 9;
        $scope.flightType = 'return';
        $scope.selectedAirportDep = null; 
        $scope.selectedAirportRet = null;
        $scope.depDate = null;
        $scope.arrDate = null;
        $scope.class = 'Economy';

        //Adult combo
        var maxAdults = maxPassengers;
        var minAdults = 1;
        $scope.adults = $filter('range')($scope.adults, minAdults, maxAdults);
        $scope.selectedAdult = 1;

        //Children combo    
        var maxChildren = maxPassengers - $scope.selectedAdult;
        var minChildren = 0;
        $scope.children = $filter('range')($scope.children, minChildren, maxChildren);
        $scope.selectedChild = 0;

        //Infant combo    
        var maxInfants = $scope.selectedAdult;
        var minInfants = 0;
        $scope.infants = $filter('range')($scope.infants, minInfants, maxInfants);
        $scope.selectedInfant = 0;

        //Populate passenger combos
        $scope.populatePassengerCombos = function () {

            //Adults combo
            $scope.adults = $filter('range')($scope.adults, minAdults, (maxPassengers - $scope.selectedChild - $scope.selectedInfant));

            //Children combo
            $scope.children = $filter('range')($scope.children, minChildren, (maxPassengers - $scope.selectedAdult - $scope.selectedInfant));

            //Infant combo
            $scope.infants = $filter('range')($scope.infants, minInfants, (maxPassengers - $scope.selectedAdult - $scope.selectedChild));
            if ($scope.infants.length > 5) {
                $scope.infants = $filter('range')($scope.infants, minInfants, 4);
            }
            if ($scope.selectedAdult < 4) {
                $scope.infants = $filter('range')($scope.infants, minInfants, $scope.selectedAdult);
            }
            if ((maxPassengers - $scope.selectedAdult - $scope.selectedChild) === 0) {
                $scope.infants = $filter('range')($scope.infants, minInfants, 0);
            }
        };

        //Error variables
        $scope.returnDateError = null;

        //Date Pickers
        //        var date = new Date();
        //        var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        $scope.calendar = {

            opened: {},
            dateFormat: 'dd/MM/yyyy',
            //            minDate: today,
            dateOptions: {},
            open: function ($event, which) {
    
                $event.preventDefault();
                $event.stopPropagation();
                $scope.calendar.opened[which] = true;
            }
        };

        //Disable return date combo when one way is selected
        $scope.disableDate = function (choice) {

            var element = angular.element('#return_date');

            if (choice === $scope.flightType) {

                element.removeClass('ng-invalid');
            } else {
                element.addClass('ng-invalid');
            }
            //True or False
            return choice === $scope.flightType;
        };

        //Validate Date
        $scope.validateDate = function () {

            var element = angular.element('#return_date');

            if ($scope.arrDate !== null && $scope.arrDate < $scope.depDate) {

                //                $scope.errorModal("Return date must be after depart date");
                $scope.returnDateError = 'Return Date cannot be eariler than Depart Date';
                $scope.arrDate = null;
                element.addClass('ng-invalid');
            } else {
                $scope.returnDateError = null;
                element.removeClass('ng-invalid');
            }
        };

        //Error modal
        $scope.errorModal = Modal.confirm.error(function (objectReturnedFromModal) {

            console.log(objectReturnedFromModal);

        });


    });