'use strict';

angular.module('angular10App')
    .config(function ($stateProvider) {
        $stateProvider
            .state('selectFlights', {
                url: '/select_flights',
                templateUrl: 'app/selectFlights/selectFlights.html',
                controller: 'SelectFlightsCtrl',
                resolve: {
                    airports: function (AirportService) {
                        return AirportService.getAirports();
                    }
                }
            });
    });
