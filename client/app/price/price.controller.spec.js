'use strict';

describe('Controller: PriceCtrl', function () {

  // load the controller's module
  beforeEach(module('angular10App'));

  var PriceCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PriceCtrl = $controller('PriceCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
