'use strict';

angular.module('angular10App')
  .config(function ($stateProvider) {
    $stateProvider
      .state('price', {
        url: '/price',
        templateUrl: 'app/price/price.html',
        controller: 'PriceCtrl'
      });
  });