'use strict';

angular.module('angular10App')
  .config(function ($stateProvider) {
    $stateProvider
      .state('modular_search', {
        url: '/modular_search',
        templateUrl: 'app/modular_search/modular_search.html',
        controller: 'ModularSearchCtrl',
        resolve: {
          airports: function (AirportService) {
            return AirportService.getAirports();
          }
        }
      });
  });
