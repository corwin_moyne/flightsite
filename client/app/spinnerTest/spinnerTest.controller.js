'use strict';

angular.module('angular10App')
    .controller('SpinnerTestCtrl', function($scope, $http, names) {

        $scope.people = names.data;

        console.log($scope.people[0]);
    });
