'use strict';

angular.module('angular10App')
    .config(function($stateProvider) {
        $stateProvider
            .state('spinnerTest', {
                url: '/spinner_test',
                templateUrl: 'app/spinnerTest/spinnerTest.html',
                controller: 'SpinnerTestCtrl',
                resolve: {

                    names: function(NamesService, $q) {

                        return NamesService.getNames().then(function(names) {

                            return names.data.length == 0 ? $q.reject('no names') : names;
                        });
                    }
                }
            });
    });
