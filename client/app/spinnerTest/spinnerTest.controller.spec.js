'use strict';

describe('Controller: SpinnerTestCtrl', function () {

  // load the controller's module
  beforeEach(module('angular10App'));

  var SpinnerTestCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SpinnerTestCtrl = $controller('SpinnerTestCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
