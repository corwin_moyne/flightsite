'use strict';

describe('Service: AirportService', function () {

  // load the service's module
  beforeEach(module('angular10App'));

  // instantiate service
  var AirportService;
  beforeEach(inject(function (_AirportService_) {
    AirportService = _AirportService_;
  }));

  it('should do something', function () {
    expect(!!AirportService).toBe(true);
  });

});
