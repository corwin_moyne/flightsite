'use strict';

angular.module('angular10App')

.factory('AirportService', function ($http) {

//    function Airport(id, country, code) {
//
//        this.id = id;
//        this.country = country;
//        this.code = code;
//    }

    return {

        getAirports: function () {

//            var airportList = [];
//            var sortedList = [];

            return $http.get('../json/airports.json')
                .then(function (response) {

                    return response.data;

//                    angular.forEach(airports, function (value, key) {
//
//                        airportList.push(new Airport(value.id, value.country, value.iata));
//                    })

//                    sortedList = _.sortBy(airportList, 'country');
//                    console.log(sortedList);
                });

//            return sortedList;
        }

        //Standard return method
        //            return {
        //                getAirports: function(){
        //                    
        //                    return $http.get('../json/airports.json');
        //                    
        //                }
    };
});