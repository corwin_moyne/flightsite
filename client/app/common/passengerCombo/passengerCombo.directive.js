'use strict';

angular.module('angular10App')
    .directive('passengerCombo', function () {
        return {
            templateUrl: 'app/common/passengerCombo/passengerCombo.html',
            restrict: 'EA',
            replace: true,
            require: 'ngModel',
            scope: {
                items: '=',
                ngModel: '=',
                populatePassengerCombos: '&'
            },
            link: function (scope, element, attrs) {
                scope.label = attrs.label;
            }
        };
    });