'use strict';

angular.module('angular10App')
    .filter('range', function () {
        return function (input, min, max) {
            var array = [];
            min = parseInt(min); //Make string input int
            max = parseInt(max);
            for (var i = min; i <= max; i++){
                array.push(i);
            }
            return array;
        };
    });