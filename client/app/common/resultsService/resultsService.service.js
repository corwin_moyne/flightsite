'use strict';

angular.module('angular10App')
    .factory('resultsService', function ($http, $filter) {

        var baseUrl = 'http://localhost:8080/itd/itd/services/';

        function createUrl($stateParams) {

            var fullRequest = baseUrl += 'getTripAvailability';
            fullRequest += '?';
            fullRequest += 'svcClass=';
            fullRequest += $filter('uppercase')($stateParams.class);
            fullRequest += '&';
            fullRequest += 'jnyReqs=';
            fullRequest += $stateParams.from;
            fullRequest += '-';
            fullRequest += $stateParams.to;
            fullRequest += '-';
            fullRequest += $filter('date')($stateParams.depDate, 'dd.MMM.yyyy');
            fullRequest += '-';
            fullRequest += ',';
            fullRequest += $stateParams.to;
            fullRequest += '-';
            fullRequest += $stateParams.from;
            fullRequest += '-';
            fullRequest += $filter('date')($stateParams.arrDate, 'dd.MMM.yyyy');
            fullRequest += '-';
            fullRequest += '&travCounts=ADULT-';
            fullRequest += $stateParams.adults;

            console.log(fullRequest);

            return fullRequest;
        }

        return {

            getResults: function ($stateParams) {

                createUrl($stateParams);

                return $http.get('../json/outbound.json')
                    .then(function (response) {

                        return response.data;
                    });
            }
        };


    });