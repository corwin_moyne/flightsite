'use strict';

describe('Service: resultsService', function () {

  // load the service's module
  beforeEach(module('angular10App'));

  // instantiate service
  var resultsService;
  beforeEach(inject(function (_resultsService_) {
    resultsService = _resultsService_;
  }));

  it('should do something', function () {
    expect(!!resultsService).toBe(true);
  });

});
