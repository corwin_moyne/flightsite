'use strict';

describe('Directive: RouteLoader', function () {

  // load the directive's module
  beforeEach(module('angular10App'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<-route-loader></-route-loader>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the RouteLoader directive');
  }));
});