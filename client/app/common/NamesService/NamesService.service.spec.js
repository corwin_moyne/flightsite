'use strict';

describe('Service: NamesService', function () {

  // load the service's module
  beforeEach(module('angular10App'));

  // instantiate service
  var NamesService;
  beforeEach(inject(function (_NamesService_) {
    NamesService = _NamesService_;
  }));

  it('should do something', function () {
    expect(!!NamesService).toBe(true);
  });

});
