'use strict';

angular.module('angular10App')
    .factory('NamesService', function($http, cfpLoadingBar) {

        var namesArray = [];

        function NamesService(firstName, lastName, tel, address, city, state, zip) {

            this.firstName = firstName;
            this.lastName = lastName;
            this.tel = tel;
            this.address = address;
            this.city = city;
            this.state = state;
            this.zip = zip;
        }

        return {

            getNames: function() {

                cfpLoadingBar.start();

                return $http.jsonp('http://www.filltext.com/?rows=10&delay=5&fname={firstName}&lname={lastName}&tel={phone|format}&address={streetAddress}&city={city}&state={usState|abbr}&zip={zip}&callback=JSON_CALLBACK')
                    .success(function(response) {

                        cfpLoadingBar.complete();

                        angular.forEach(response, function(value, key) {

                            namesArray.push(new NamesService(value.fname, value.lname, value.tel, value.address, value.city, value.state, value.zip));
                        })
                    });
            }
        }

        return namesArray;
    });
