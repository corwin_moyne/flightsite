'use strict';

angular.module('angular10App')
  .factory('SearchFlightObject', function () {

    var searchFLightObject = function(){

      this.flightType = null;
      this.outboundAirport = null;
      this.inboundAirport = null;
      this.departureDate = null;
      this.returnDate = null;
      this.cabin = null;
      this.adults = null;
      this.children = null;
      this.infants = null;
    }

    return searchFLightObject;
  });
