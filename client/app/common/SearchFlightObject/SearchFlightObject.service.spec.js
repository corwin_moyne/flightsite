'use strict';

describe('Service: SearchFlightObject', function () {

  // load the service's module
  beforeEach(module('angular10App'));

  // instantiate service
  var SearchFlightObject;
  beforeEach(inject(function (_SearchFlightObject_) {
    SearchFlightObject = _SearchFlightObject_;
  }));

  it('should do something', function () {
    expect(!!SearchFlightObject).toBe(true);
  });

});
