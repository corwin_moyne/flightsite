'use strict';

angular.module('angular10App', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ui.router',
  'ui.bootstrap',
    'ngStorage',
    'cfp.loadingBar',
    'ngAnimate'
])
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    
    $urlRouterProvider
            .otherwise('/');

        $locationProvider.html5Mode(true);
    });